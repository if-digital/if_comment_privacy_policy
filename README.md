# if.comment Accept Privacy Policy WordPress Plugin
Erweitert das Standard-Kommentarfeld um Checkbox zur Zustimmung der Datenschutzbestimmungen.

Download ZIP: https://bitbucket.org/if-digital/if_comment_privacy_policy/downloads/

# Features
- Neue Kommentare werden ohne IP-Adresse gespeichert
- Zusätzliche Checkbox zum akzeptieren der Datenschutzbestimmungen
- Validation ob Checkbox gesetzt wurde

![Screenshot 1](https://bitbucket.org/if-digital/if_comment_privacy_policy/raw/c1998e29ad39f3b9126bd5a6da036f6faf803ccc/screenshot_01.jpg)
![Screenshot 2](https://bitbucket.org/if-digital/if_comment_privacy_policy/raw/c1998e29ad39f3b9126bd5a6da036f6faf803ccc/screenshot_02.jpg)

#Installation
- Plugin als ZIP-File herunterladen
- Klicke im WordPress Dashboard auf Plugins > Installieren.
- Klicke auf Plugin hochladen.
- Klicke auf Durchsuchen.
- Wähle die ZIP-Datei aus, die du heruntergeladen hast und wähle Öffnen.
- Klicke auf Jetzt installieren. ...
- Klicke auf Aktviere dieses Plugin.
