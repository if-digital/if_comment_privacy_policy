<?php
/*
Plugin Name: if.comment Accept Privacy Policy
Version: 1.0
Plugin URI: https://www.if.digital/
Description: if.comment add additional field for Privacy Policy
Author: if.digital // Felix Wiesel
Author URI: https://www.if.digital/
*/


/* #################################### */
// remove_commentsip
/* #################################### */

function ifcomment_remove_commentsip( $comment_author_ip ) {
	return '';
}
add_filter( 'pre_comment_user_ip', 'ifcomment_remove_commentsip' );

/* #################################### */
// add field
/* #################################### */

add_action('comment_form_logged_in_after', 'additional_fields');
add_action('comment_form_after_fields', 'additional_fields');

function additional_fields(){

	echo '<span class="if-comment-accept-privacy">';
    echo '<label class="control-label" for="Privacy">' . __('Zustimmung Datenschutzbestimmungen')  . '<span class="required">*</span></label><br>';
	echo '<input id="Privacy" name="Privacy" type="checkbox" class="form-control" required /> Hiermit akzeptiere ich die Datenschutzbestimmungen.';
	echo '</span>';
}

/* #################################### */
// save option
/* #################################### */

add_action('comment_post', 'save_comment_meta_data');
function save_comment_meta_data($comment_id)
{

    if ((isset($_POST['Privacy'])) && ($_POST['Privacy'] != ''))
        $Privacy = wp_filter_nohtml_kses($_POST['Privacy']);
    add_comment_meta($comment_id, 'Privacy', $Privacy);

}

/* #################################### */
// validation
/* #################################### */

add_filter('preprocess_comment', 'verify_comment_meta_data');
function verify_comment_meta_data($commentdata)
{
  if (!isset($_POST['Privacy']))
      wp_die(__('Fehler: Bitte akzeptieren Sie unsere Datenschutzbestimmungen.'));
  return $commentdata;
}

/* #################################### */
// show in backend
/* #################################### */

add_action('add_meta_boxes_comment', 'extend_comment_add_meta_box');
function extend_comment_add_meta_box()
{
    add_meta_box('Privacy', __('Accept Privacy Policy'), 'extend_comment_meta_box', 'comment', 'normal', 'high');
}

function extend_comment_meta_box($comment)
{
    $Privacy = get_comment_meta($comment->comment_ID, 'Privacy', true);
    wp_nonce_field('extend_comment_update', 'extend_comment_update', false);

	echo '<table class="form-table editcomment"><tbody><tr>';
	    echo '<td class="first"><label for="Privacy">Zustimmung Datenschutzbestimmungen</label></td>';
		if($Privacy == 'on'){
			echo '<td>✔ Nutzer hat den Datenschutzbestimmungen <strong>zugestimmt</strong>.</td>';
		}else{
			echo '<td>✖ Nutzer hat den Datenschutzbestimmungen <strong>nicht zugestimmt</strong>.</td>';
		}
    echo '</tr></tbody></table>';
}
